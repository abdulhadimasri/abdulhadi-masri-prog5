import React, { Component } from 'react';
import './index.css';
import TableMetSC from './TableMetSC';
import Table from './state/Table';
import Form  from  './Form/Form';

class AppComponent extends Component {

  render() {
    return (
      <div className="App">
        <h1>Een React App maken met een component</h1>
        <TableMetSC />
      </div>
    );
  }
}
/*
class AppApi extends Component {
    state = {
        data: []
    };

    // Code is invoked after the component is mounted/inserted into the DOM tree.
    componentDidMount() {
        const url = "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=YourAPIKey here....";

        // alles over fetch:
        // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
        fetch(url)
            .then(result => result.json())
            .then(result => {
                this.setState({
                    data: result.data.results
                })
            }).catch(function (result)
                {alert('Error: ' + result);} );
    }

    render() {
        const { data } = this.state;
        // styling the react way
        const thumbnail = {
               height: "100px"
        };
        const result = data.map((character) => {
            let src = `${character.thumbnail.path}.${character.thumbnail.extension}`;
            // eslint-disable-next-line
            let alt = `Afbeelding van ${character.name}`;
            return <tr key={character.id}>
                    <td>{character.name}</td>
                    <td><img style={thumbnail} src={src} alt={src}/></td>
                   </tr>;
        });
        return <table>{result}</table>;
    }
}
*/
class AppForm extends Component{
  removeAlbum = index => {

    const albums = this.state.albums.filter((album, i) => {
      return i !== index

    });
    this.setState({ albums });
  }
  state = {
    albums:[]
  };
 
  render(){
    return(
      <div className ="container">
        <Table caption="Upstairs Downstairs karakters" auteur="AbdulHadi" data = {this.state.albums} removeAlbum = {this.removeAlbum} />
      <Form handelSubmit={this.handelSubmit}/>
      </div>
    );
  }

  handelSubmit = album =>{
    this.setState({albums:[...this.state.albums, album]})
  }
}
/*
class AppState extends Component {
      state = { albums:  [{
                'nummer': '6',
                'naam': 'Het hemelhuis ',
                'cover': 'Softcover',
                'prijs': '&euro; 5.22'
            },
            {
                'nummer': '7',
                'naam': 'De zwarte Bomma',
                'cover': 'Softcover',
                'prijs': '&euro; 5.22'
            },
            {
                'nummer': '8',
                'naam': 'De ooievaar van Begonia',
                'cover': 'Softcover',
                'prijs': '&euro; 5.22'
            },
            {
                'nummer': '9',
                'naam': 'De Schildpaddenschat',
                'cover': 'Softcover',
                'prijs': '&euro; 5.22'
            },
            {
                'nummer': '10',
                'naam': 'De straalvogel',
                'cover': 'Hardcover',
                'prijs': '&euro; 8.22'
            },
            {
                'nummer': '11',
                'naam': 'De Zonnemummie',
                'cover': 'Softcover',
                'prijs': '&euro; 5.22'
            }
        ]};
        
    removeAlbum = index => {
        const albums = this.state.albums.filter((album, i) => {
            return (i !== index);
        });
        this.setState({albums});
    }
  render() {
    return (
      <div className="App">
        <h1>Een React App maken met een component en state</h1>
        <Table caption="Upstairs Downstairs karakters" auteur="JI" removeAlbum={this.removeAlbum}
          data={this.state.albums}/>
      </div>
    );
  }
}
*/
export default AppComponent && AppForm ;
