import React, {Component} from 'react';
import caption, { TFooter, THead, Caption } from './TableSC';
class Table extends Component {
    render() {
        return (
            <table>
                <caption>Jommeke's strips (eigen component)</caption>
                <thead>
                    <tr>
                        <th scope="col">Nummer</th>
                        <th scope="col">Titel</th>
                        <th scope="col">Kaft</th>
                        <th scope="col">&euro;</th>
                    </tr>
                </thead>                
                <tfoot>
                    <tr>
                        <th scope="col">Nummer</th>
                        <th scope="col">Titel</th>
                        <th scope="col">Kaft</th>
                        <th scope="col">&euro;</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Jacht op een voetbal</td>
                        <td>Softcover</td>
                        <td>&euro; 5.22</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>De zingende aap</td>
                        <td>Softcover</td>
                        <td>&euro; 5.22</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>De Koningin van Onderland</td>
                        <td>Hardcover</td>
                        <td>&euro; 8.22</td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>Purperen pillen</td>
                        <td>Softcover</td>
                        <td>&euro; 5.22</td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>De Muzikale Bella</td>
                        <td>Hardcover</td>
                        <td>&euro; 8.22</td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default Table;