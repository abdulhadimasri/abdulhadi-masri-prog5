import React, {Component} from 'react';

class Table extends Component {
    render() {
        const data = this.props.data;
        return (
            <div>
            <table>
                <caption>{this.props.caption}</caption>
                    {data.map(function (row, index) {
                        return (
                            <tr>
                                <td>{index}</td>
                                <td>{row.nummer}</td>
                                <td>{row.naam}</td>
                                <td>{row.cover}</td>
                                <td>{row.prijs}</td>
                            </tr>)
                    })}
                
            </table>
            <p><cite className="auteur">{this.props.auteur}</cite></p>
            </div>);
    }
}

export default Table;