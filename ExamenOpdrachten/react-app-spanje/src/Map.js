import React, {Component} from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';

const Wrapper = styled.div`
    width: ${props => props.width};
    height: ${props => props.height};
  `;
 
 export default class Map extends Component {
 Lat = this.props.data.lat;
 Long= this.props.data.long;
 
     componentDidMount(){
         const map = L.map('map', {
             center : [41.476338,1.993735],
             zoom:10,
             zoomControl:false
         });
         
         L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            detectRetina: true,
            maxZoom: 20,
            maxNativeZoom: 17,
         }).addTo(map);
          var circle= L.circle([this.Long,this.Lat], {
              color:'black',
              fillColor:'black',
              radius:1000
          }).addTo(map);
            circle.bindPopup(this.props.data.name);
         
         
     }
     
     render() {
         return <Wrapper width="550px" height="480px" id="map" />
     }
     
 }
    