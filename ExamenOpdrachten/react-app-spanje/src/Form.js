import React, { Component } from 'react';

export default class Form extends Component {
    handleChange = event => {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }
    constructor(props) {
        super(props);

        this.initialState = {
            comment: ''
        };
        this.state = this.initialState;
    }
    render() {
        const { comment } = this.state;
        return (
            <form >
                <label>comment</label>
                <input className="comment"
                    type="text"
                    name="comment"
                    value={comment}
                    onChange={this.handleChange} />
                <input
                    type="button"
                    value="Add Comment"
                    onClick={this.submitForm} />  
            </form>
        );
    }
    submitForm = () => {
        this.props.handleSubmit(this.state);
        this.setState(this.initialState);
    }
  
}