import React, {Component} from 'react';
import data from './data/Barcelona.json';

class List extends Component {
    list = data.Barcelona;
    
    
    
    row = this.list.map(item => {
        let url = `/images/Barcelona/${item.image}`;
        let alt = `foto van ${item.name}`;
        return (
            <tr>
                <td><img src={url} alt={alt} /></td>
                <td>{item.name}</td>
                <td><button onClick={() => this.props.action('Detail', item)}>Detail</button></td>
            </tr>
        )
    })
    render() {
        return (
            <table>
               {this.row}
            </table>
            );
            
    }
}

export default List;