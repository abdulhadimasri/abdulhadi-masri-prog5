import React, {Component} from 'react';



export default class Table extends Component {


render(){
        const comment = this.props.comment;
        return(
            <div >
                <table align="center" className="tablee">
                    {comment.map((row,index) => {
                        return(
                        <tr>
                                <td >{row.comment}</td>
                                <td><button onClick={() =>this.props.removeComment(index)}>Delete</button></td>
                        </tr>)
                    })}
                </table>
            </div>
        );
    }
}