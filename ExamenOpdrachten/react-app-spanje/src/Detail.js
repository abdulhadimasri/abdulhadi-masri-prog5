import React, {Component} from 'react';
import Map from './Map';
import Table from './Table';
import Form from './Form';
class Detail extends Component {
    removeComment = index => {

    const comments = this.state.comments.filter((comment, i) => {
      return i !== index

    });
    this.setState({ comments });
  }
  state = {
    comments:[]
  };
    
    render() {
        let url = `/images/Barcelona/${this.props.data.image}`;
        let alt = `foto van ${this.props.data.name}`;
        
        return (
            
        <div align="center">
                <h1> {this.props.data.name} Details</h1>
            <div align="right">
                <button className="btnn" onClick={() => this.props.action('List')}>Back</button>
            </div>
                
            <div id="img">
                <img src={url} alt={alt}/>
            </div>
            <div className="details">
                <div><b>City:</b> {this.props.data.city}</div>
                <div><b>Description:</b> {this.props.data.Description}</div>
            </div>
            <div id="mapid">
                {<Map data={this.props.data} />} 
            </div>
            <div className="Comments">
                <Table className="tablee" comment = {this.state.comments} removeComment = {this.removeComment}/>
                <Form handleSubmit={this.handleSubmit} /> 
            </div>
        </div>
             

        )
    }
    
    handleSubmit = comment =>{
    this.setState({comments:[...this.state.comments,comment]})
    }
}
export default Detail;