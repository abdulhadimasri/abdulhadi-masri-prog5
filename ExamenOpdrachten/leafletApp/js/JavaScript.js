const myMap = L.map('map');
const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});
myBasemap.addTo(myMap);
myMap.setView([51.196118, 4.417881], 12);
const request = new XMLHttpRequest();
request.open('GET', 'js/map.json', true);
request.onload = function() {
    const data = JSON.parse(this.response);
    const books = data.books.map(book => {
        L.marker([book.lat, book.long]).bindPopup(`
        <h2> ${book.name}</h2>
        <p><b>Price:</b> ${book.Price}</p>
        <p><b>Comments:</b> ${book.comments}</p>
    `).openPopup().addTo(myMap);
    });
    
     const individualNeighborhood = data.books.filter(book => {});

    const neighborhoodCount = data.books.reduce((sums, book) => {
        sums[book.neighborhood] = (sums[book.neighborhood] || 0) + 1;
        return sums;
    }, {});

    const sidebar = document.getElementById("neighborhoods");
    for(let neighborhood  in neighborhoodCount){
        const p = document.createElement("p");
        p.innerHTML = `<b>${neighborhood}</b> :${neighborhoodCount[neighborhood]}`;
        sidebar.appendChild(p);
    }

}
request.send();
