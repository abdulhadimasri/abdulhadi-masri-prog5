function accumalator() {
    var totaal = 0;
    var bewerking;
    var bewerkingen = '+-*/^';
    var getal = 0;
    let optellen = function(getal) {
        totaal += getal;
    };

    let aftrekken = function(getal) {
        totaal -= getal;
    };

    let product = function(getal) {
        totaal *= getal;
    };

    let deling = function(getal) {
        totaal /= getal;
    };

    let macht = function(getal) {
        totaal = totaal + Math.pow(totaal, getal);
    };

    return {
        execute : function(content) {
        // als content in de bewerkingen string zit
        if (bewerkingen.indexOf(content) >= 0) {
            bewerking = content;
        }
        else if (content == '=') {
            // alleen als er eerst een bewerking is gekozen.
            if (bewerking !== undefined) {
                switch (bewerking) {
                    case '+':
                        optellen(getal);
                        break;
                    case '-':
                        aftrekken(getal);
                        break;
                    case '*':
                        product(getal);
                        break;
                    case '/':
                        deling(getal);
                        break;
                    case '^':
                        macht(getal);
                        break;
                }
            }
         }
        else {
            getal = parseInt(content);
        }
    },
    getTotaal : function() {return totaal;},
    getGetal : function() {return getal;}
    }
}

let deurneAccumulator = accumalator('.numeric-keypad');
let oostendeAccumalor = accumalator('.numeric-keypad');
let numericKeypadController = function(e) {
    if (e.target.tagName == 'DIV') {
        // alleen als op een div in de numeric-keybad container
        // wordt geklikt.
        if (e.target.parentNode.className == 'numeric-keypad') {
            let content = e.target.innerText;
            let totaal = deurneAccumulator.execute(content);
            document.querySelector('#totaal-bedrag').innerText = deurneAccumulator.getTotaal();
            document.querySelector('#display').innerText = deurneAccumulator.getGetal();
       }
    }
}

let numericKeypad = document.querySelector('.numeric-keypad');
numericKeypad.addEventListener('click', numericKeypadController, false);
