var scrollDelay = 2000;
var marqueeSpeed = 1;
var timer;

var scrollArea;
var marquee;
var scrollposition = 0;

var scrolling = function() {
    if (scrollposition + scrollArea.offsetHeight <=0) {
        scrollposition = marquee.offsetHeight;
    }
    else {
        scrollposition = scrollposition - marqueeSpeed;
    }
    scrollArea.style.top = scrollposition + "px";
}


var startScrolling = function() {
    /*scrollArea = document.getElementById("scrol;-area");
    scrollArea.style.top = 0;
    marquee = document.getElementById("marquee");
    setTimeout(startScrolling, scrollDelay);*/
    timer = setInterval(scrolling, 30);
}

var intializeMarquee = function() {
    scrollArea = document.getElementById("scroll-area");
    scrollArea.style.top =  0;
    marquee = document.getElementById("marquee");
    setTimeout(startScrolling,scrollDelay);
}

var pausemarquee = function() {
    if (marqueeSpeed > 0) {
        marqueeSpeed = 0;
    }
    else {
        marqueeSpeed = 1;
    }
}

var speedUpMarquee = function() {
    if (marqueeSpeed <= 3) {
        marqueeSpeed++;
    }
}

var slowDwonMarquee = function() {
    if (marqueeSpeed > 1) {
        marqueeSpeed--;
    }
}

window.onload = intializeMarquee;