var scrollDelay = 2000;
var marqueeSpeed = 1;
var timer;

var scrollArea;
var marquee;
var scrollPosition = 0;

var scrolling = function () {
    if (scrollPosition + scrollArea.offsetHeight <= 0) {
        scrollPosition = marquee.offsetHeight;
    }
    else {
        scrollPosition = scrollPosition - marqueeSpeed;

    }
    scrollArea.style.top = scrollPosition + "px";
}

var startScrolling = function () {
    timer = setInterval(scrolling, 30);
}

var initializeMarquee = function () {
    scrollArea = document.getElementById("scroll-area");
    //scrollArea.style.top =10;
     marquee = document.getElementById("marquee");
    setTimeout(startScrolling, scrollDelay);
}

   let divElement1 = document.getElementById('marquee');
   divElement1.addEventListener('mouseover', pauseMarquee ,false);
   divElement1.addEventListener('mouseout', pauseMarquee, false);
    function pauseMarquee () {
    if (marqueeSpeed > 0) {
       marqueeSpeed = 0;
    } else {
       marqueeSpeed = 1;
    }
}
let snelbutton = document.querySelector('#val1');
snelbutton.addEventListener('click',speedUpMarquee,false);  
 function speedUpMarquee (ev) {
    if (marqueeSpeed <= 3) {
        marqueeSpeed++;
    }
}
let traagbutton = document.querySelector('#val2');
traagbutton.addEventListener('click', slowDownMarquee, false);
 function slowDownMarquee () {
    if (marqueeSpeed > 1) {
        marqueeSpeed--;
    }
}


window.onload = initializeMarquee;