import React, {Component} from 'react';

class Detail extends Component {
    commentList = null;
    state = {'commentList' : this.commentList};

    commentListUI = function() {
       this.commentList = this.state.commentList;
       if(this.commentList === null) {
            this.commentList = ['Geen'];
        }
        let commentUI = this.commentList.map(item => {
            // alert(item);
            return (
                <p class="comment">
                    <strong>{item.UserName}</strong>: {item.Comment}
                </p>
            )
        });
        return commentUI;
    }

    // Code is invoked after the component is mounted/inserted into the DOM tree.
    componentDidMount() {
        const prepUrl = `https://abdulhadi95-abdulhadi95.c9users.io/mmt-php-api/public/curiositycomment/readallwhere?CuriosityId=${this.props.data.key}`;
        // alert(prepUrl);
        const url = prepUrl;

        // alles over fetch:
        // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
        fetch(url, {
            credentials: 'omit'  
        })
            .then(result => {
                // alert(result);
                return result.json()
                })
            .then(result => {
                // alert(JSON.stringify(result));
                this.setState({
                    commentList: result
                })
            }).catch(function (result)
                {alert('Error: ' + result);} );
    }

    
    render() {
        return (
            <article>
               <h1>Detail {this.props.data.name}</h1>
               <button onClick={() => this.props.action('List')}>List</button>
               <fieldset>
                   <div>
                    <label for="comment">Commentaar:</label>
                    <textarea id="comment"></textarea>
                   </div>
                   <button onClick={() => this.props.action('DetailAddComment',
                        document.getElementById('comment').value, this.props.data)}>Opslaan</button>
               </fieldset>
               <div>
                {this.commentListUI()}

               </div>
             </article>
        )
    }
}

export default Detail; 