var myContacts = [
    { "name": "Hadi", "email": "hadi@gmail.com", "mobile": "0032154154"},
    { "name": "Masri", "email": "Masri@gmail.com", "mobile": "0032154454"},
    { "name": "Abdul", "email": "Abdul@gmail.com", "mobile": "0032124854"}
    
];

function generateDynamicTable(){
    var noOfContacts = myContacts.length;
    
    if(noOfContacts>0){
        //CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        table.style.width= '50%';
        table.setAttribute('border',1);
        table.setAttribute('cellspacing', '0');
        table.setAttribute('cellpadding', '5');
        
        //retrieve column header (Name, email, mobile)
        
        var col =[];
        for (var i =0; i< noOfContacts; i++) {
            for (var key in myContacts[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }
        
        //Create table head
        var tHead =  document.createElement("thead");
        //create row for table head
        var hRow =  document.createElement("tr");
        
        // ADD COLUMN HEADER TO ROW OF TABLE HEAD.
			for (var i = 0; i < col.length; i++) {
					var th = document.createElement("th");
					th.innerHTML = col[i];
					hRow.appendChild(th);
			}
			tHead.appendChild(hRow);
			table.appendChild(tHead);
			
			// CREATE TABLE BODY .
			var tBody = document.createElement("tbody");	
			
			// ADD COLUMN HEADER TO ROW OF TABLE HEAD.
			for (var i = 0; i < noOfContacts; i++) {
			
					var bRow = document.createElement("tr"); // CREATE ROW FOR EACH RECORD .
					
					
					for (var j = 0; j < col.length; j++) {
						var td = document.createElement("td");
						td.innerHTML = myContacts[i][col[j]];
						bRow.appendChild(td);
					}
					tBody.appendChild(bRow)

			}
			table.appendChild(tBody);	
			
			
			// FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
			var divContainer = document.getElementById("myContacts");
			divContainer.innerHTML=""
			divContainer.appendChild(table);
			
    }
}
